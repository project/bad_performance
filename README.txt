Bad Performance

This module causes terrible performance, and you can actually configure just how bad the performance is.

If you break your site with this module, simply delete the entries from the variables table that start with "bad_performance".  If you're not sure how to do that, you probably shouldn't be using this module.
